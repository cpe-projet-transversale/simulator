package model;

import java.util.List;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import utils.Request;

@JsonSerialize(using = SensorSerializer.class) //permet d'indiquer la classe qui transforme l'objet en json 
public class Sensor {
	int id;
    Position position;
    float intensity;

    /**
     * Retourne la position du capteur
     * @return Position : latitude/longitude du capteur
     */
	public Position getPos() {
		return position;
	}

	public float getIntensity() {
		return this.intensity;
	}

	public void setIntensity(float newIntensity) {
        this.intensity = newIntensity;
	}

	public int getId() {
		return this.id;
	}

	@JsonCreator // constructor can be public, private, whatever
	private Sensor(
		@JsonProperty("id") int id,
		@JsonProperty("lat") double latitude,
		@JsonProperty("long") double longitude,
		@JsonProperty("value") int intensity)
	{
		this.id = id;
		this.position = new Position(latitude, longitude);
		this.intensity = intensity;
	}
}