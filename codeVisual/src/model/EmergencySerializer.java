package model;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class EmergencySerializer extends StdSerializer<Emergency> {
    public EmergencySerializer() {
        this(null);
    }

    public EmergencySerializer(Class<Emergency> t){
        super(t);
    }

    @Override
    public void serialize(Emergency value, JsonGenerator jgen, SerializerProvider provider)
    throws IOException, JsonProcessingException
    {
        jgen.writeStartObject();
        jgen.writeNumberField("value", value.getIntensity());
        //jgen.writeStringField("itemName", value.getIntensity());
        jgen.writeNumberField("lat", value.getPosition().getLatitude());
        jgen.writeNumberField("long", value.getPosition().getLongitude());
        jgen.writeEndObject();
    }
}