package model;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class SensorSerializer extends StdSerializer<Sensor> {
    public SensorSerializer() {
        this(null);
    }

    public SensorSerializer(Class<Sensor> t){
        super(t);
    }
    @Override
    public void serialize(Sensor value, JsonGenerator jgen, SerializerProvider provider)
    throws IOException, JsonProcessingException
    {
        jgen.writeStartObject();
        //jgen.writeStringField("itemName", value.getIntensity());
        jgen.writeNumberField("lat", value.getPos().getLatitude());
        jgen.writeNumberField("long", value.getPos().getLongitude());
        jgen.writeNumberField("value", value.getIntensity());
        /*if(value.emergencyId == 0)
            jgen.writeNullField("emergencyId");
        else
            jgen.writeNumberField("emergencyId", value.emergencyId);*/
        jgen.writeEndObject();
    }
}
