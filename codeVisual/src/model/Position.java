package model;

public class Position {
    private double latitude;
    private double longitude;

    public Position(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
/*
	public Service getNearest() {
		return null;
    }
    */
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Position) //Si on compare avec une autre position
            return (((Position)obj).latitude == this.latitude && ((Position)obj).longitude == this.longitude); //Compare lat/long
        return super.equals(obj); //Sinon comparaison habituelle
    }
}
