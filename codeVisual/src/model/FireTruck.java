package model;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = FireTruckSerializer.class) //permet d'indiquer la classe qui transforme l'objet en json 
@JsonIgnoreProperties(ignoreUnknown = true) //Permet d'ignorer les inconnus en provenance du JSON
public class FireTruck extends Service {

	int id;
	//Intervention assignedIntervention = null;
	int assignedFireStationId;
	//List<Crew> assignedCrew = new ArrayList<Crew>();
	boolean returnToBase;
	int emergencyId;
	double speed = 0.001;
/*
	public void setFree() {
		this.setIntervention(null);
	}*/

	public boolean getReturnToBase() {
		return this.returnToBase;
	}

	public int getId() {
		return this.id;
	}

	public int getAssignedFireStationId() {
		return this.assignedFireStationId;
	}


	public Position getPos() {
		return this.position;
	}

/*	public List<Crew> getCrew() {
		return this.assignedCrew;
	}
/*
	public void assignNecessaryCrew() {
		Crew.getFreeCrewFromFireStation(this.assignedFireStation.getId());
	}*/
/*
	public Intervention getIntervention() {
		return this.assignedIntervention;
	}*//*

	public void setIntervention(Intervention intervention){
		this.assignedIntervention = intervention;
	}*/

	public void moveTo(Position pos) {


		double difLat;
		double difLong;
		
		// Déplacement des camions petit à petit
		if (!this.getPos().equals(pos)){
			difLat = this.getPos().getLatitude() - pos.getLatitude();
			difLong = this.getPos().getLongitude() - pos.getLongitude();
			
			//Move en lat
			if (Math.abs(difLat) > this.speed) {
				if (difLat > 0){
					this.getPos().setLatitude(this.getPos().getLatitude()-this.speed);
				} else {
					if (difLat < 0){
						this.getPos().setLatitude(this.getPos().getLatitude()+this.speed);
					}
				} 
			}
			//Move en long
			if (Math.abs(difLong) > this.speed) {
				if (difLong > 0){
					this.getPos().setLongitude(this.getPos().getLongitude()-this.speed);
				}
				else {
					if (difLong < 0){
						this.getPos().setLongitude(this.getPos().getLongitude()+this.speed);
					}
				} 
			}    
			if (Math.abs(difLat)<=this.speed && Math.abs(difLong)<=this.speed){
				System.out.println("TP sur la position pour camion " + this.getId());
				this.position = pos;
			}
		}
		//this.position = pos;
	}
/*
	public static List<FireTruck> getAllFireTruckForIntervention(int id) {
		Request.request(null, "GET");
		//SELECT * FROM FireTruck WHERE assignedIntervention LIKE id
		return null;
	}

	public static List<FireTruck> getTrucksWithCrew() {
		Request.request(null, "GET");
		//SELECT * FROM FireTruck WHERE id not in (SELECT * FROM Crew WHERE associatedFireTruckId IS NOT NULL)
		return null;
	}

	public static List<FireTruck> getAllFireTruck() {
		Request.request(null, "GET");
		return null;
	}*/

	@JsonCreator // constructor can be public, private, whatever
	private FireTruck(
        @JsonProperty("id") int id,
        @JsonProperty("lat") double latitude,
		@JsonProperty("long") double longitude,
		@JsonProperty("fireStationId") int fireStationId,
		@JsonProperty("emergencyId") int emergencyId)
	{
		this.id = id;
		this.position = new Position(latitude, longitude);
		this.assignedFireStationId = fireStationId;
		this.emergencyId = emergencyId;
	}

}
