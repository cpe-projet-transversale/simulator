package task;

import java.util.Random;
import java.util.TimerTask;

import controller.GameManager;
import model.Emergency;
import model.Position;
import model.Sensor;



public class createNewFire extends TimerTask {
    /**
     * Pourcentage de chance qu'un feu se déclare
     */
    private float percentageChanceOfFireEachTurn = 15;
    
    private GameManager gameManager;

    public createNewFire(GameManager gameManager) {
        this.gameManager = gameManager;
    }
    
    /**
     * Décide si un feu doit être créé ou non (random)
     * @return boolean Si un feu doit être créé ou non
     */
    private boolean shouldCreateNewFire(){
        double rand = Math.random() * 100; //On crée un nombre random
        if ((rand -= percentageChanceOfFireEachTurn ) < 0) return true; //Pas de chance ! On crée un feu
        else return false; //On ne crée pas de feu
    }

    private Position generatePositionRandomly(){
        double minLat = 45.737700;
        double maxLat = 45.766050;
        Random r = new Random();
        double latitude = minLat + (maxLat - minLat) * r.nextDouble();
        //double latitude = minLat + (double)(Math.random() * ((maxLat - minLat) + 1));
        double minLon = 4.804010;
        double maxLon = 4.885470;  
        Random r2 = new Random();
        double longitude = minLon + (maxLon - minLon) * r2.nextDouble();
        //double longitude = minLon + (double)(Math.random() * ((maxLon - minLon) + 1));
        System.out.println("Coordonnées du nouveau feu : Lat = "+latitude+", Long = "+longitude);
        return new Position(latitude, longitude);
    }

    @Override
    public void run() {
        if(shouldCreateNewFire()){ //Si on doit créer un nouveau feu
            int numberOfSensors = gameManager.getListSensor().size(); //On récupère le nombre total de capteurs
            Random ran1 = new Random();  //On génère un chiffre aléatoire pour désigner un capteur
            int randomSensorId = ran1.nextInt(numberOfSensors)+1; // la fonction est exclusive, mais on démarre à 1 dans la BDD. Donc on rajoute 1 pour faire de 1 à 60
            
            Random ran = new Random(); //On génère un chiffre aléatoire pour l'intensité du feu
            int randomIntensity = ran.nextInt(gameManager.getMaxIntensity() ); // la fonction est exclusive, mais on démarre aussi à 0 pour nos intensités. Donc on rajoute pas +1
            //On modifie l'intensité du feu du capteur
            Sensor designatedSensor = gameManager.getSensorById(randomSensorId); //On récupère le capteur
            if(designatedSensor == null) return; //failsafe
            //Sensor designatedSensor = gameManager.getListSensor().get(randomSensorId);
            if(designatedSensor.getIntensity() <= randomIntensity){ //Si le capteur a une intensité inférieur à celui généré
                designatedSensor.setIntensity(randomIntensity); //On modifie son intensité.
                /*Emergency emergency = null;
                for(Emergency e : gameManager.getListEmergency()){ //On cherche l'existence d'un feu sur ce capteur
                    if(e.getPosition().equals(designatedSensor.getPos())){
                        emergency = e;
                    }
                }
                if (emergency == null) { //si on en a pas trouvé
                    emergency = new Emergency(0,designatedSensor.getPos(), randomIntensity); //On crée un feu à la position du capteur
                    gameManager.getListEmergency().add(emergency);
                    System.out.println("Created new fire");
                }
                else{ //sinon on met à jour son intensité
                    //System.out.println("Changed fire intensity");
                    //emergency.setIntensity(randomIntensity);
                }*/
            }
            System.out.println("Génération de feu d'intensité "+ randomIntensity);
            //Emergency emergency = new Emergency(0,this.generatePositionRandomly(), randomIntensity);
            
        }
        else 
            System.out.println("We should not create a fire : "+percentageChanceOfFireEachTurn+"% chance)");
        
    }
}