package task;

import java.util.TimerTask;

import controller.GameManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.Emergency;
import model.FireTruck;
import model.Intervention;
import model.Position;



public class moveFireTruck extends TimerTask {  
    
    private GameManager gameManager;

    public moveFireTruck(GameManager gameManager) {
        this.gameManager = gameManager;
    }
    
    
    @Override
    public void run() {
        Map <Emergency,ArrayList<FireTruck>> map = gameManager.getMapEmergencyFireTruck();
        List<FireTruck> listFt = new ArrayList<>();
        for (Map.Entry<Emergency, ArrayList<FireTruck>> entry : map.entrySet()) { //Pour chaque camion lié à une intervention
            for (FireTruck truck : entry.getValue()) {
                if(!listFt.contains(truck)) //On ajoute tous les camions qui bougent à un feu
                    listFt.add(truck);
                Emergency emergency = entry.getKey();
                System.out.println(truck.getId() + " go to emergency " + emergency.getId());
                truck.moveTo(emergency.getPosition());
            }
        }


        for (FireTruck fireTruck : gameManager.getListFireTruck()) {
            if(!listFt.contains(fireTruck)){ //Si le camion ne bouge pas à un feu, retour à la caserne
                System.out.println(fireTruck.getId() + " returns to base");
                fireTruck.moveTo((gameManager.getFireStationById(fireTruck.getAssignedFireStationId())).getPos());
            }
        }
        //System.out.println("Moving all trucks without intervention to their fireStation");
    }
}