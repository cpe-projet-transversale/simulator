package task;

import java.util.TimerTask;

import controller.GameManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.Emergency;
import model.FireTruck;
import model.Sensor;



public class increaseFireIntensity extends TimerTask {    
    private GameManager gameManager;
    private int maxNbrTurnBeforeIncrease =  4;

    public increaseFireIntensity(GameManager gameManager) {
        this.gameManager = gameManager;
    }
    
    @Override
    public void run() {
        List<Emergency> listE = gameManager.getListEmergency();
        Map<Emergency,ArrayList<FireTruck>> map = gameManager.getMapEmergencyFireTruck();
        
        for(Emergency emergency : listE){
            boolean truckIsOnPos = false;
            if(map.containsKey(emergency)){
                for (Map.Entry<Emergency, ArrayList<FireTruck>> entry : map.entrySet()) { 
                    for (FireTruck truck : entry.getValue()) {
                        if (truck.getPos().equals(emergency.getPosition())){
                            truckIsOnPos = true;
                        }
                    }
                }
            }
            if(!truckIsOnPos){
                //On augmente l'intensité du feu qui n'a pas de camion sur sa position
                if(emergency.getTurnWithoutReduce() <= this.maxNbrTurnBeforeIncrease){
                    emergency.setTurnWithoutReduce(emergency.getTurnWithoutReduce()+1);
                }
                else { //sinon on reset & augmente l'intensité
                    emergency.setTurnWithoutReduce(0);
                        //emergency.setIntensity(emergency.getIntensity()+1);
                        //On MàJ le capteur associé
                    for(Sensor s : gameManager.getListSensor()){
                        if(s.getIntensity() < gameManager.getMaxIntensity()){
                            if(s.getPos().equals(emergency.getPosition())){
                                s.setIntensity(s.getIntensity()+1);
                            }
                        }
                    }
                }
            }
        }
    }
}