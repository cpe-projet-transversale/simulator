package task;

import java.util.TimerTask;

import com.fasterxml.jackson.core.JsonProcessingException;

import controller.GameManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.Emergency;
import model.FireTruck;
import model.Intervention;
import utils.Request;

public class updateDatabase extends TimerTask {
    private GameManager gameManager;

    public updateDatabase(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @Override
    public void run() {
        System.out.println("*** MAJ BDD ***");
        try {
            //Request.postEmergency(gameManager.getListEmergency());
            Request.patchSensor(gameManager.getListSensor());
            Request.patchFireTrucksPositions(gameManager.getListFireTruck());
            Request.updateEmergencyList(gameManager.getListEmergency());
            gameManager.updateMap();
        } catch (JsonProcessingException e) {
            System.out.println("Failed to update DB");
            e.printStackTrace();
        }
        System.out.println("--- TURN ENDS ---");
       /* System.out.println("*** STATE ***");
        List<Emergency> list = gameManager.getListEmergency();
        for (Emergency emergency : list) {
            System.out.println(emergency.getId() + " : " + emergency.getIntensity());
        }
        System.out.println("*** STATE ***");*/
    }
}