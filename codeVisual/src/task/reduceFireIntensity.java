package task;

import java.util.TimerTask;

import controller.GameManager;

import java.util.ArrayList;
import java.util.Map;

import model.Emergency;
import model.FireTruck;
import model.Sensor;



public class reduceFireIntensity extends TimerTask {    
    private GameManager gameManager;
    //private int maxNbrTurnBeforeIncrease =  6;

    public reduceFireIntensity(GameManager gameManager) {
        this.gameManager = gameManager;
    }
    
    @Override
    public void run() {
        System.out.println("--- TURN BEGINS ---");
        Map<Emergency,ArrayList<FireTruck>> map = gameManager.getMapEmergencyFireTruck();
        //List<Emergency> emergencyToRemove = new ArrayList<>();
        for (Map.Entry<Emergency, ArrayList<FireTruck>> entry : map.entrySet()) { 
            for (FireTruck truck : entry.getValue()) {
                Emergency emergency = entry.getKey();
                if (truck.getPos().equals(emergency.getPosition())){//On réduit l'intensité du feu par camion présent sur le lieu de l'incendie
                    
                        //emergency.setIntensity(emergency.getIntensity()-1);
                        //On MàJ le capteur associé
                    for(Sensor s : gameManager.getListSensor()){
                        if(s.getIntensity() > 0){
                            if(s.getPos().equals(emergency.getPosition())){
                                s.setIntensity(s.getIntensity()-1);
                            }
                        }
                    }
                    //Le remove de l'emergency se fait dans le post des emergency
                    /*else{ // Si l'intensité est à 0 on supprime le feu
                        emergencyToRemove.add(emergency);
                        gameManager.getListEmergency().remove(emergency); 
                    }*/
                }
                
                else {
                    //On augmente l'intensité du feu qui n'a pas de camion sur sa position
                   /*/ if(emergency.getTurnWithoutReduce() <= this.maxNbrTurnBeforeIncrease){
                        emergency.setTurnWithoutReduce(emergency.getTurnWithoutReduce()+1);
                    }
                    else {
                        emergency.setTurnWithoutReduce(0);
                        if(emergency.getIntensity() < gameManager.getMaxIntensity()){
                            //emergency.setIntensity(emergency.getIntensity()+1);
                            //On MàJ le capteur associé
                            for(Sensor s : gameManager.getListSensor()){
                                if(s.getPos().equals(emergency.getPosition())){
                                    s.setIntensity(emergency.getIntensity()+1);
                                }
                            }
                        }
                    }*/
                }
                
            }
        }
    }
}