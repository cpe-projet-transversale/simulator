package controller;

import java.util.Timer;
import java.util.TimerTask;

import task.*;

public class Simulator {
    
    public static void main(String[] args) {
        /**
         * Délai d'attente entre chaque tâche, en millisecondes
         */
        int delayForTask = 2000;
        int numberOfTask = 5;

        GameManager gameManager = new GameManager();

        TimerTask moveFireTruckTask = new moveFireTruck(gameManager);
        TimerTask createNewFireTask = new createNewFire(gameManager);
        TimerTask reduceFireIntensityTask = new reduceFireIntensity(gameManager);
        TimerTask increaseFireIntensityTask = new increaseFireIntensity(gameManager);
        TimerTask updateDatabaseTask = new updateDatabase(gameManager);
        Timer timer = new Timer(true); //On utilise qu'un seul timer pour qu'elle s'éxécute séquentiellement... Semble logique mais bonne idée pour les tours ? (durée d'éxécution de chaque code)
        timer.scheduleAtFixedRate(reduceFireIntensityTask, delayForTask - (numberOfTask)*(delayForTask/numberOfTask), delayForTask); //On commence la réduction des feux directement, puis toutes les 2 secondes
        timer.scheduleAtFixedRate(increaseFireIntensityTask, delayForTask - (numberOfTask-1)*(delayForTask/numberOfTask), delayForTask); //On commence la création des feux à partir de 2 secondes, toutes les deux secondes
        timer.scheduleAtFixedRate(moveFireTruckTask, delayForTask - (numberOfTask-2)*(delayForTask/numberOfTask), delayForTask); 
        timer.scheduleAtFixedRate(createNewFireTask, delayForTask - (numberOfTask-3)*(delayForTask/numberOfTask), delayForTask); //On commence la création des feux à partir de 2 secondes, toutes les deux secondes
        timer.scheduleAtFixedRate(updateDatabaseTask, delayForTask - (numberOfTask-4)*(delayForTask/numberOfTask), delayForTask); 
                
        try {
            Thread.sleep(120000); //On attends 60 ms avant de tout annuler pour stop le programme
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        timer.cancel();
    }
}